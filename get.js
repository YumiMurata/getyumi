const url= 'https://in-api-treinamento.herokuapp.com/posts'

let lista=document.querySelector("#lista");

fetch(url)
.then(respostaRequisicao => {
    return respostaRequisicao.json()
})
.then( (listaDeObjetos)=>{
    listaDeObjetos.map(objeto => {
        let elemento = document.createElement("li");
        elemento.innerText = objeto.name +" - "+ objeto.message;
        lista.appendChild(elemento);
    });
} )

